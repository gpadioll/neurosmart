"use strict";

var NS = NS || {};


/**
 * Object regrouping various utilities
 */
NS.Utilities = function(){
};

/**
 * Clamp value between min and max
 * @param {number} value - initial value
 * @param {number} min - minimal value
 * @param {number} max - maximal value
 */
NS.Utilities.clamp = function clamp(value, min, max){
    return Math.min(Math.max(value, min), max);
};

/**
 * Parses float number from string
 *
 * @param {string} stringValue - string to be parsed
 * @param {string} defaultValue - default value if no number value detected
 */
NS.Utilities.getFloatFromString = function getFloatFromString(stringValue, defaultValue){
	if (! isNaN(stringValue)) {
        return parseFloat(stringValue);
    }
    return defaultValue;
};

/**
 * Returns URL-parameter value
 *
 * @param {string} sParam - param to extract from URL params
 */
NS.Utilities.getUrlParameter = function getUrlParameter(sParam) {
	var sPageURL = decodeURIComponent(window.location.search.substring(1)),
		sURLVariables = sPageURL.split('&'),
		sParameterName,
		i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : sParameterName[1];
		}
	}
};

/**
 * Loads the file specified at the url fileUrl then calls the callback
 * The callback method takes the downloaded resource as parameter
 *
 * @param {string} fileUrl - URL of the file
 * @param {object} caller - reference to caller object (parent of callback and errorCallback)
 * @param {function} callback - function to be called when loading is complete
 * @param {function} errorCallback - function to be called when loading failed
 *
 */
NS.Utilities.asyncLoadTextFile = function(fileUrl, caller, callback, errorCallback){
	var request;
	if(window.XMLHttpRequest){
		request = new XMLHttpRequest();
		// Here we load plain text files
		request.overrideMimeType('text/plain');
	} else if(window.ActiveXObject){
		request = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	request.onreadystatechange = function(){
		if(request.readyState === 4) {
			if (request.status === 200) {
				// Success
				callback.call(caller, request.responseText);
			}  else if ((request.status === 404) || (request.responseText === "")) {
				// Error
				errorCallback.call(caller, fileUrl);
			}
		}
	};
	request.open('GET', fileUrl, true);
	request.send();
};
